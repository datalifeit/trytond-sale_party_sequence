# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .sale import Sale, Party, PartySaleSequence


def register():
    Pool.register(
        Party,
        PartySaleSequence,
        Sale,
        module='sale_party_sequence', type_='model')
