datalife_sale_party_sequence
============================

The sale_party_sequence module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-sale_party_sequence/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-sale_party_sequence)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
